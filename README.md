# SOLAR PLUS
## This Mod Was created by Dewta and Ace!

This Mod Will add Solar banks to crafting, a magazine skill to craft solar cells and banks (200 for balancing), solar cells should cost 1000 forged steel and 100 electrical parts for level 1, then 2000, 200, all the way up to 7k and 700. This aims to keep it difficult to get solar cells, but without having to farm traders for days... Hope you all enjoy

Things this mod **will** change:

- Added Solar banks to workbench crafting via magazine skill (Greener Living Magazine).
- Added solar cells to crafting (via Magiazine skill).
- Magazine skill unlocks solar cells level 1-6 at magazine level 100, 120, 140, 160, 180, and 200.
- Solar banks Unlock at Magazine Skill 50.
- adds a skill tab for Solar Plus and the skill "Greener Living" which increases magazine chance.
- Magazines are ONLY lootable via zombie lootbags, this helps make them harder to come by.

Things this mod will **NOT** change:

- Solar cell trader rates
- Solar cell efficiency
- solar bank assets

## Support

Our discord server will be open to support for our mods and our mods only, we do not answer support requests that ask us to Add compatibility with other mods. We only design our modpacks with our mods in mind.
**https://discord.gg/9rxDH9EYjs**

## Delevopment

Our team is working really hard on making these mods bug free and fun, and yes, we do plan on updating to A22 if it came to it. As for previous Alphas, unfortunately we do not have any plans on making variants of our mods to work with older game versions.

**Development/UI Design** - Dewta <br>
**Development/Graphical/UI Design** - Ace

If you would like to support us, our link will be below, donating is not required, just helps me make more mods. Thank you! <br>
<a href="https://www.paypal.com/paypalme/Dellta55">Paypal</a>


## License

If you modify this work both authors must be credited. And you are not allowed to post this mod.

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/3.0/deed.en_US">Creative Commons Attribution 3.0 Unported License</a>.
